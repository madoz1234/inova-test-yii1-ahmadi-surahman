-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 30, 2021 at 07:01 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klinik`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `diskon` varchar(10) NOT NULL DEFAULT '0',
  `keuntungan` varchar(20) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang_dalam`
--

CREATE TABLE `tbl_barang_dalam` (
  `id_barang_dalam` int(10) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `stok` int(10) NOT NULL,
  `harga_pokok` int(10) NOT NULL,
  `harga_jual` int(10) NOT NULL,
  `diskon` int(10) NOT NULL,
  `keuntungan` int(10) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dokter`
--

CREATE TABLE `tbl_dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `tempat_tanggal_lahir` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `no_telepon` varchar(150) NOT NULL,
  `spesialis` varchar(150) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master_transaksi`
--

CREATE TABLE `tbl_master_transaksi` (
  `id_master_transaksi` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_perawatan` int(11) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `hutang` int(11) NOT NULL DEFAULT 0,
  `biaya` int(11) NOT NULL DEFAULT 0,
  `total` int(11) NOT NULL DEFAULT 0,
  `total_bayar` int(11) NOT NULL DEFAULT 0,
  `status_pembayaran` varchar(20) NOT NULL DEFAULT 'Belum Lunas'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_obat_perawatan`
--

CREATE TABLE `tbl_obat_perawatan` (
  `id_obat_perawatan` int(10) NOT NULL,
  `id_perawatan` int(10) NOT NULL,
  `id_obat` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pasien`
--

CREATE TABLE `tbl_pasien` (
  `id_pasien` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `tempat_tanggal_lahir` varchar(100) NOT NULL,
  `no_telepon` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `member` varchar(5) NOT NULL DEFAULT 'Tidak'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pasien`
--

INSERT INTO `tbl_pasien` (`id_pasien`, `nama`, `alamat`, `tempat_tanggal_lahir`, `no_telepon`, `created_at`, `member`) VALUES
(3, 'asdsad', 'sadsadas', 'dddd', '213232', '2021-05-30 23:56:20', 'Ya');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengaturan`
--

CREATE TABLE `tbl_pengaturan` (
  `id_pengaturan` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `title` varchar(150) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pengaturan`
--

INSERT INTO `tbl_pengaturan` (`id_pengaturan`, `type`, `title`, `content`) VALUES
(1, 'site_name', 'Nama Aplikasi', 'Sistem Manajemen Klinik');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengeluaran`
--

CREATE TABLE `tbl_pengeluaran` (
  `id_pengeluaran` int(10) NOT NULL,
  `pengeluaran` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `total` int(10) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengeluaran`
--

INSERT INTO `tbl_pengeluaran` (`id_pengeluaran`, `pengeluaran`, `jumlah`, `total`, `created_at`, `updated_at`) VALUES
(1, 1111, 1222, 3333, '2021-05-30 13:58:26', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perawat`
--

CREATE TABLE `tbl_perawat` (
  `id_perawat` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `tempat_tanggal_lahir` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_telepon` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perawatan`
--

CREATE TABLE `tbl_perawatan` (
  `id_perawatan` int(11) NOT NULL,
  `nama_perawatan` varchar(150) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon_member` varchar(5) NOT NULL,
  `diskon_umum` varchar(5) NOT NULL,
  `komisi_dokter` int(11) NOT NULL,
  `komisi_perawat` int(11) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_dokter`
--

CREATE TABLE `tbl_transaksi_dokter` (
  `id_transaksi_dokter` int(11) NOT NULL,
  `id_master_transaksi` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi_dokter`
--

INSERT INTO `tbl_transaksi_dokter` (`id_transaksi_dokter`, `id_master_transaksi`, `id_dokter`, `created_at`) VALUES
(39, 2, 1, '22/11/2014 04:49:30'),
(40, 2, 2, '22/11/2014 04:49:30'),
(55, 4, 2, '2014-11-24 00:13:19'),
(61, 5, 2, '2014-11-24 00:15:07'),
(62, 6, 1, '2014-11-27 00:19:09'),
(78, 8, 1, '2014-12-01 22:19:53'),
(105, 3, 1, '2014-12-03 00:11:16'),
(106, 3, 2, '2014-12-03 00:11:16'),
(107, 9, 1, '2021-05-30 15:24:25'),
(108, 10, 1, '2021-05-30 15:25:27'),
(109, 11, 1, '2021-05-30 15:25:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_obat`
--

CREATE TABLE `tbl_transaksi_obat` (
  `id_transaksi_obat` int(11) NOT NULL,
  `id_master_transaksi` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT 0,
  `created_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi_obat`
--

INSERT INTO `tbl_transaksi_obat` (`id_transaksi_obat`, `id_master_transaksi`, `id_obat`, `jumlah`, `created_at`) VALUES
(37, 2, 1, 1, '22/11/2014 04:49:30'),
(52, 4, 1, 20, '2014-11-24 00:13:19'),
(58, 5, 2, 5, '2014-11-24 00:15:07'),
(59, 6, 2, 10, '2014-11-27 00:19:09'),
(60, 6, 1, 1, '2014-11-27 00:19:09'),
(62, 7, 2, 1, '2014-11-30 10:42:54'),
(87, 8, 1, 10, '2014-12-01 22:19:53'),
(88, 8, 1, 5, '2014-12-01 22:19:53'),
(115, 3, 1, 10, '2014-12-03 00:11:16'),
(116, 3, 1, 10, '2014-12-03 00:11:16'),
(117, 9, 1, 1, '2021-05-30 15:24:25'),
(118, 9, 1, 1, '2021-05-30 15:24:25'),
(119, 10, 1, 1, '2021-05-30 15:25:27'),
(120, 10, 1, 1, '2021-05-30 15:25:27'),
(121, 11, 1, 1, '2021-05-30 15:25:36'),
(122, 11, 1, 1, '2021-05-30 15:25:36');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_perawat`
--

CREATE TABLE `tbl_transaksi_perawat` (
  `id_transaksi_perawat` int(11) NOT NULL,
  `id_master_transaksi` int(11) NOT NULL,
  `id_perawat` int(11) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi_perawat`
--

INSERT INTO `tbl_transaksi_perawat` (`id_transaksi_perawat`, `id_master_transaksi`, `id_perawat`, `created_at`) VALUES
(40, 2, 1, '22/11/2014 04:49:30'),
(59, 4, 1, '2014-11-24 00:13:19'),
(65, 5, 1, '2014-11-24 00:15:07'),
(66, 6, 1, '2014-11-27 00:19:09'),
(84, 8, 1, '2014-12-01 22:19:53'),
(124, 3, 1, '2014-12-03 00:11:16'),
(125, 3, 1, '2014-12-03 00:11:16'),
(126, 3, 1, '2014-12-03 00:11:16'),
(127, 9, 1, '2021-05-30 15:24:25'),
(128, 10, 1, '2021-05-30 15:25:27'),
(129, 11, 1, '2021-05-30 15:25:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_perawatan`
--

CREATE TABLE `tbl_transaksi_perawatan` (
  `id_transaksi_perawatan` int(10) NOT NULL,
  `id_master_transaksi` int(10) NOT NULL,
  `id_perawatan` int(10) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_cms`
--

CREATE TABLE `tbl_user_cms` (
  `id` int(11) NOT NULL,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kasir',
  `telepon` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user_cms`
--

INSERT INTO `tbl_user_cms` (`id`, `username`, `password`, `nama`, `email`, `status`, `telepon`) VALUES
(3, 'admin', '$2a$10$M0Z3.tA3.fv/KIUN0Ck6OO8bX7e7d.ZE7EGRE8.H0ig5qlUUVT9jO', 'Admin', 'owner@mail.com', 'admin', '0987654321');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tbl_barang_dalam`
--
ALTER TABLE `tbl_barang_dalam`
  ADD PRIMARY KEY (`id_barang_dalam`);

--
-- Indexes for table `tbl_dokter`
--
ALTER TABLE `tbl_dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `tbl_master_transaksi`
--
ALTER TABLE `tbl_master_transaksi`
  ADD PRIMARY KEY (`id_master_transaksi`);

--
-- Indexes for table `tbl_obat_perawatan`
--
ALTER TABLE `tbl_obat_perawatan`
  ADD PRIMARY KEY (`id_obat_perawatan`);

--
-- Indexes for table `tbl_pasien`
--
ALTER TABLE `tbl_pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `tbl_pengaturan`
--
ALTER TABLE `tbl_pengaturan`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indexes for table `tbl_pengeluaran`
--
ALTER TABLE `tbl_pengeluaran`
  ADD PRIMARY KEY (`id_pengeluaran`);

--
-- Indexes for table `tbl_perawat`
--
ALTER TABLE `tbl_perawat`
  ADD PRIMARY KEY (`id_perawat`);

--
-- Indexes for table `tbl_perawatan`
--
ALTER TABLE `tbl_perawatan`
  ADD PRIMARY KEY (`id_perawatan`);

--
-- Indexes for table `tbl_transaksi_dokter`
--
ALTER TABLE `tbl_transaksi_dokter`
  ADD PRIMARY KEY (`id_transaksi_dokter`);

--
-- Indexes for table `tbl_transaksi_obat`
--
ALTER TABLE `tbl_transaksi_obat`
  ADD PRIMARY KEY (`id_transaksi_obat`);

--
-- Indexes for table `tbl_transaksi_perawat`
--
ALTER TABLE `tbl_transaksi_perawat`
  ADD PRIMARY KEY (`id_transaksi_perawat`);

--
-- Indexes for table `tbl_transaksi_perawatan`
--
ALTER TABLE `tbl_transaksi_perawatan`
  ADD PRIMARY KEY (`id_transaksi_perawatan`);

--
-- Indexes for table `tbl_user_cms`
--
ALTER TABLE `tbl_user_cms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_barang_dalam`
--
ALTER TABLE `tbl_barang_dalam`
  MODIFY `id_barang_dalam` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_dokter`
--
ALTER TABLE `tbl_dokter`
  MODIFY `id_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_master_transaksi`
--
ALTER TABLE `tbl_master_transaksi`
  MODIFY `id_master_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_obat_perawatan`
--
ALTER TABLE `tbl_obat_perawatan`
  MODIFY `id_obat_perawatan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pasien`
--
ALTER TABLE `tbl_pasien`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_pengaturan`
--
ALTER TABLE `tbl_pengaturan`
  MODIFY `id_pengaturan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pengeluaran`
--
ALTER TABLE `tbl_pengeluaran`
  MODIFY `id_pengeluaran` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_perawat`
--
ALTER TABLE `tbl_perawat`
  MODIFY `id_perawat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_perawatan`
--
ALTER TABLE `tbl_perawatan`
  MODIFY `id_perawatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_transaksi_dokter`
--
ALTER TABLE `tbl_transaksi_dokter`
  MODIFY `id_transaksi_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `tbl_transaksi_obat`
--
ALTER TABLE `tbl_transaksi_obat`
  MODIFY `id_transaksi_obat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `tbl_transaksi_perawat`
--
ALTER TABLE `tbl_transaksi_perawat`
  MODIFY `id_transaksi_perawat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `tbl_transaksi_perawatan`
--
ALTER TABLE `tbl_transaksi_perawatan`
  MODIFY `id_transaksi_perawatan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user_cms`
--
ALTER TABLE `tbl_user_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
