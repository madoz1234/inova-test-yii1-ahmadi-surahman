<?php
/* @var $this PerawatController */
/* @var $model Perawat */

$this->breadcrumbs=array(
	'Perawat'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Data Perawat', 'url'=>array('index')),
	array('label'=>'Tambah Perawat', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#perawat-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">
          Data Perawat
        </h3>
      </div>
      <div class="card-body pad table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'perawat-grid',
	'itemsCssClass'=>'table table-bordered',
	'dataProvider'=>$model->search(),
   'template'=>'{items}{pager}<br>{summary}',
	'columns'=>array(
	     array(
	      'header'=>'No',
	      'type'=>'raw',
	      'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
	      ),
		'nama',
		'tempat_tanggal_lahir',
		'alamat',
		'no_telepon',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
</div>
</div>
</div>
</div>