<?php
/* @var $this PasienController */
/* @var $model Pasien */

$this->breadcrumbs=array(
	'Pasien'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Data Pasien', 'url'=>array('index')),
	array('label'=>'Tambah Pasien', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pasien-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">
          Data Pasien
        </h3>
      </div>
      <div class="card-body pad table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pasien-grid',
	'itemsCssClass'=>'table table-bordered',
	'dataProvider'=>$model->search(),
   'template'=>'{items}{pager}<br>{summary}',
	'columns'=>array(
	     array(
	      'header'=>'No',
	      'type'=>'raw',
	      'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
	      ),
		'nama',
		'alamat',
		'no_telepon',
		'member',
		array(
			'class' => 'CButtonColumn',
			'template' => ' {Detail Perawatan}',
			  'buttons'=>array
			    (
			        'Detail Perawatan' => array
			        (
			            'url'=>'Yii::app()->createUrl("history_pasien/by_pasien", array("id"=>$data["id_pasien"]))',
			        ),
			    ),
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
 </div>
</div>
</div>
</div>
</div>
