<?php
/* @var $this PasienController */
/* @var $model Pasien */
/* @var $form CActiveForm */
?>
<div class="container-fluid">
<div class="form-group">
  <div class="col-md-12">
  	<div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Pasien</h3>
      </div>
    	<div class="card-body">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pasien-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>150, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'alamat'); ?>
		<?php echo $form->textArea($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tempat_tanggal_lahir'); ?>
		<?php echo $form->textField($model,'tempat_tanggal_lahir',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'tempat_tanggal_lahir'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'no_telepon'); ?>
		<?php echo $form->textField($model,'no_telepon',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'no_telepon'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'member'); ?>
        <?php echo $form->dropDownList($model,'member',array("Ya"=>"Ya","Tidak"=>"Tidak")); ?>
		<?php echo $form->error($model,'member'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
</div>
</div>
</div>
</div>