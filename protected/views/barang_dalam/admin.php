<?php
/* @var $this Barang_dalamController */
/* @var $model BarangDalam */

$this->breadcrumbs=array(
	'Barang Dalam'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Data Barang Dalam', 'url'=>array('index')),
	array('label'=>'Tambah Barang Dalam', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#barang-dalam-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">
          Data Barang Dalam
        </h3>
      </div>
      <div class="card-body pad table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'barang-dalam-grid',
	'itemsCssClass'=>'table table-bordered',
	'dataProvider'=>$model->search(),
   'template'=>'{items}{pager}<br>{summary}',
	'columns'=>array(
	     array(
	      'header'=>'No',
	      'type'=>'raw',
	      'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
	      ),
		'nama_barang',
		'stok',
		'harga_pokok',
		'harga_jual',
		'diskon',
		'keuntungan',
		/*
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
</div>
</div>
</div>
</div>
