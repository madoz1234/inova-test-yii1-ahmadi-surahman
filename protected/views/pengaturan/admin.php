<?php
/* @var $this PengaturanController */
/* @var $model Pengaturan */

$this->breadcrumbs=array(
	'Pengaturan'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Data Pengaturan', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pengaturan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">
          Setting
        </h3>
      </div>
      <div class="card-body pad table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pengaturan-grid',
	'itemsCssClass'=>'table table-bordered',
	'dataProvider'=>$model->search(),
   'template'=>'{items}{pager}<br>{summary}',
	'columns'=>array(
	     array(
	      'header'=>'No',
	      'type'=>'raw',
	      'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
	      ),
		'type',
		'title',
		'content',
		array(
			'class' => 'CButtonColumn',
			'template' => '{Edit}',
			  'buttons'=>array
			    (
			        'Edit' => array
			        (
			            'url'=>'Yii::app()->createUrl("pengaturan/update", array("id"=>$data["id_pengaturan"]))',
			        ),
			    ),		
		),
	),
)); ?>
</div>
</div>
</div>
</div>
</div>
