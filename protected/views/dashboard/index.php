<?php
/* @var $this DashboardController */

$this->breadcrumbs=array(
	'Dashboard',
);
?>
<h1>
    Dashboard - <?php echo $_SESSION['site_name']; ?> 
    <?php 
        if(Yii::app()->user->status === 'kasir' || Yii::app()->user->status === 'owner'){
    ?>
    <a href="<?php echo Yii::app()->baseUrl; ?>/history_pasien/create" class="btn btn-warning pull-right">Tambah Transaksi</a>
    <?php
        }
    ?>
</h1>
<div class="container-fluid" id="content-area">
    <div class="row">
        <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?php echo $pasien ?></h3>
                    <p>Data Pasien</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user"></i>
                </div>
                <a href="<?php echo Yii::app()->baseUrl; ?>/pasien" class="small-box-footer">Selengkapnya<i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
                <div class="inner">
                    <h3><?php echo $obat ?></h3>
                    <p>Data Obat</p>
                </div>
                <div class="icon">
                    <i class="fas fa-medkit"></i>
                </div>
                <a href="<?php echo Yii::app()->baseUrl; ?>/barang" class="small-box-footer">Selengkapnya<i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3><?php echo $dokter ?></h3>
                    <p>Data Dokter</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-md"></i>
                </div>
                <a href="<?php echo Yii::app()->baseUrl; ?>/dokter" class="small-box-footer">Selengkapnya<i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3><?php echo $rm ?></h3>
                    <p>Rekam Medis</p>
                </div>
                <div class="icon">
                    <i class="fas fa-stethoscope"></i>
                </div>
                <a href="<?php echo Yii::app()->baseUrl; ?>/history_pasien" class="small-box-footer">Selengkapnya<i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
               'options'=>array(
                  'title' => array('text' => 'Grafik Kunjungan Pasien - '.date('M/Y')),
                  'xAxis' => array(
                     'categories' => $arr_date,
                     'title' => array('text' => 'Tanggal')
                  ),
                  'yAxis' => array(
                     'title' => array('text' => 'Jumlah Pasien')
                  ),
                  'series' => array(
                     array('name' => 'Jumlah Pasien', 'data' => $arr_total)
                  )
               )
            ));
            ?>
        </div>
    </div>
</div>
